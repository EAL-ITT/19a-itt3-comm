---
title: '19A ITT3 Communication protocols'
subtitle: 'Lecture plan'
#authors: ['Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait18, 19A
* Name of lecturer and date of filling in form: MON \today
* Title of the course, module or project and ECTS: Elective course on Communication protocols, 5 ECTS
* Required readings, literature or technical instructions and other background material: A good understanding of networking, services and packages as taught on ITT1 and ITT2

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


| Teacher |  Week | Content |
| :---: | :---: | :--- |
|  MON | 35 | Introduction, tools and online resources |
|  MON | 36 | HTTP(S) and webdav |
|  MON | 37 | Telnet, SSH |
|  MON | 38 | DNS, DNS over TLS, DNS over HTTPS |
|  MON | 39 | (S)SMTP, IMAP(S), POP3(S), SPF, DKIM, DMARC |
|  MON | 40 | VPN, IPSEC, OpenVPN, wireguard |
|  MON | 41 | Exams |
|  MON | 42 | Vacation |




# General info about the course, module or project

The course is about application layer protocols, and we will be working with them in order to understand them and use them properly.

## The student’s learning outcome

At the end of the course the student will be able to

* set up a test environment for sniffing traffic
* monitor traffic to test that the protocols used are the ones intented
* explain and use relevant encrypted protols
* find protocol documentation in e.g. RFCs


## Content

Each week a new protocol is analysed. The basic template wil be for the students to set up a sniffing environment, look at the data transmitted and received and compare it to the relevant standards.

## Method

There will be a minimum of teacher lectures, and a lot of student presentations. Collaboration is expected.

## Equipment

The students will need laptop, and possibly (at least) a VM with kali linux.

## Projects with external collaborators  (proportion of students who participated)

None at this time.

## Test form/assessment

The exam in this course is a 2 hour practical exam graded as pass or fail.

The students will be given an exercise in a protocol and must be able to extract information and find online sources to support them in their analysis.

See exam catalogue for details on compulsory elements.

## Other general information
None at this time.
