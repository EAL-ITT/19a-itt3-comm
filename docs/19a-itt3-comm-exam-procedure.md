---
title: '19A ITT3 Communication protocols'
subtitle: 'Examination procedure'
#authors: ['Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: Examination procedure
skip-toc: true
---

# The exam

The examination date is **Friday October 11th** from 9:00 to 11:00. Room is juniper lab in B2.19.

The exam is a two hour practical exam, graded passed/not passed. There will be a number of practical and theoretical exercises, that the student must be able to perform and answer.

Arrive in good time, so you can patch in your laptop and check that everything is working as you expect.

At the the end of the examination, a document is handed in on wiseflow and is the foundation for the exam grade.

# The bring list

The student must bring

* a laptop
* pen and paper for notes
* a functioning Kali virtual machine or have tools available by other means

# A word on practical exams

Please note that you will be working with live systems and you will be able to disrupt the examination of your class mates. Behave properly and we'll a nice exam. Misbehavior will be regarded as either cheating or sabotage, and appropriate action will be taken.
