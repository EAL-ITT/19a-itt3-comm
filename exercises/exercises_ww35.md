# Exercises for ww35

Tooling up

## Exercise 1: Install Kali

### Information

Have a functioning Kali installation in Vmware or whatever your prefered hypervisor is.

### Exercise instructions

1. Go to the [download site](https://www.kali.org/downloads/) for Kali

    You probably need a 64 bit version.

2. Run it and check that it works.

One of the groups will do a show'n'tell for the class

### Links

See above


## Exercise 2: RFCs


### Information

RFC are the basic building blocks for internet communication.

### Exercise instructions

1. Go to the [official RFC site](https://tools.ietf.org/rfc/index)

2. Browse it, google it and so on to get an idea about what RFCs are and how it works.

3. Pick a topic like DNS, HTTP, or another one from the topcis list in the course plan.

4. Look at it to get a idea of the content of the specfic RFC.

5. Evaluate whether or not it matches your previous understanding of the topic.

6. Review your notes and make them usefullfor next time.

7. Repeat 3-6


One of the groups will do a show'n'tell for the class about part 2.


### Links

see above


## Exercise 3: Kali tools


### Information

Kali has a lot of tools.


### Exercise instructions

1. Start at [the tools list](https://tools.kali.org/tools-listing) or use google to pick a cool tool that matches your favorite topic from exercise 2.

    Obscure and/or specialized tools are ok.

2. Design a way of testing the tools, and decide what you expect to get out of it.

3. Use the tool

4. Compare with your expectation.

5. Do a demo.

6. Repeat 1-5.

One or two groups will do a demo for the class.


### Links

see above
