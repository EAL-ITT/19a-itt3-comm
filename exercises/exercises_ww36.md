\pagebreak

# Exercises for ww36

HTTP and HTTPS

## Exercise 1: RFC and HTTP

RFCs are the building block, also for HTTP.

### Exercise instructions

1. Make a list of all RFCs involving HTTP
2. Add 1-2 sentences to each about what they cover

   "I don't get it" is acceptable for some of the obscure technical ones.

3. Read [RFC2616](https://tools.ietf.org/html/rfc2616) in detail

   Focus on

   * Terminology
   * Return codes
   * Methods
   * Header fields

4. Formulate your take on HTTP

   * Half a page with appropriate link to the RFC
   * Focus on what yo ufind most relevant, interesting or you understand the best
   * upload to gitlab

### Links

see above


## Exercise 2: Install a webserver

We will use Nginx and the [Webdav](https://tools.ietf.org/html/rfc4918) module.

### Exercise instructions

1. Install Nginx and webdav in a VM

    Installation instructions/hint are [available](https://starbeamrainbowlabs.com/blog/article.php?article=posts%2F237-WebDav-Nginx-Setup.html)

2. Decide how to test, do it and document.


We are not using let's encrypt unless someone does some magic with the SRX240.


### Links

see above


## Exercise 3: Sniffing HTTP

We want to refind the method, headers and such from the RFC.


### Exercise instructions

1. Using Kali, connect to the webserver to check connectivity

2. Start wireshark

3. Connect to the webserver and use proper filters in wireshark and isolate the traffic to and from the webserver.

4. Look in the kali toolbox for relevant webdav tools

    Like [davtest](https://tools.kali.org/web-applications/davtest), or perhaps [this](https://greysec.net/showthread.php?tid=1294) is relevant.

5. Refind the HTTP from the webdav connection. Take notes, save pcaps, and describe result.


### Links

see above


## Exercise 4: HTTPS

Again, we are not using let's encrypt unless someone does some magic with the SRX240.



### Exercise instructions

1. create a self-signed for nginx and enable https.

    It is fairly simple, when you get the mechanism, e.g. see [humankode.com](https://www.humankode.com/ssl/create-a-selfsigned-certificate-for-nginx-in-5-minutes)

2. As before, connect to the webserver, refind HTTP and HTTPS in wireshark. Take notes, **save pcaps**, and describe result.

    We expect to get a lot of (unreadable) encrypted stuff with structure and handshakes.

3. As before, connect using webdav. Take notes, **save pcaps**, and describe result.

### Links

see above


## Exercise 5a: Decrypting - GUI

Since you have the private key of the server, you should be able to decrypt the traffic.

### Exercise instructions

1. E.g. see [accedian.com](https://accedian.com/enterprises/blog/how-to-decrypt-an-https-exchange-with-wireshark/) and use wireshark to decode the encrypted traffic you saved earlier.

2. Take notes and screenshots.


### Links

See above.


## Exercise 5b: Decrypting - CLI

No instructions, look at [ssldump](https://github.com/adulau/ssldump). Perhaps start [with a guide](https://packetpushers.net/using-ssldump-decode-ssltls-packets/)
