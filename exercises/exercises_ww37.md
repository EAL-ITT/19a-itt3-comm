\pagebreak

# Exercises for ww37

Telnet and SSH


## Exercise 1: Telnet


### Exercise instructions

1. Look into [rfc584](https://tools.ietf.org/html/rfc854) and [rfc5198](https://tools.ietf.org/html/rfc5198) (the update related to unicode chars)
   and [rfc698](https://tools.ietf.org/html/rfc698) for extra credit
1. Set up a VM with telnet service enabled
   e.g. use `apt-get install telnetd`
2. Connect to it using the kali VM and refind the connection in wireshark
3. Compare telnet [util usage](https://www.computerhope.com/unix/utelnet.htm) with the RFC
   Is `AUTOLOGIN` working?


### Links

* [python telnet library](https://docs.python.org/3/library/telnetlib.html)


## Exercise 2: SSH


### Exercise instructions

1. Read [rfc4251](https://tools.ietf.org/html/rfc4251), [rfc4252](https://tools.ietf.org/html/rfc4252), [rfc4253](https://tools.ietf.org/html/rfc4253) and  [rfc4254](https://tools.ietf.org/html/rfc4254).
   Paraphrase how ssh works.

2. Read [rfc8308](https://tools.ietf.org/html/rfc8308).
   Is it relevant?
   Can we use the [`elevation`](https://tools.ietf.org/html/rfc8308#ref-elevation) mechanism? (Spoiler: I don't find anything usefull)

### Links

see above


## Exercise 3: Configure an ssh server

There a lot of possible security options for an ssh server.

### Exercise instructions:

1. In a (non-kali) VM, install the ssh server (`apt-get install openssh-server`)
2. Review congifuration [options](https://www.ssh.com/ssh/sshd_config/)
3. Configure the server to accept only ssh keys, and disallow root logins.
4. (optional) limit login to specific users.
5. Test [sslyse](https://tools.kali.org/information-gathering/sslyze). Is it usefull in this context?

Test and document.

### Links

see above


## Exercise 4a: Set up Mitm attack

This is a common attack. MITM is described [here](https://www.ssh.com/attack/man-in-the-middle)

### Exercise instructions:

1. Make [this](https://securityonline.info/ssh-mitm-ssh-man-middle-tool/) work
   or [this](https://andrewmichaelsmith.com/2014/03/quick-and-easy-ssh-mitm/)

### Links

see above

## Exercise 4b: SSL strip

[SSL strip](https://moxie.org/software/sslstrip/) is cool

### Exercise instructions:

1. Readup on ssl strip
2. Make [this](https://gbhackers.com/mitm-attack-https-connection-ssl-strip/) work
   or [this](https://samsclass.info/123/proj10/p21-sslstrip.html). Notice the use of proxy.

### Links

see above

## Exercise 4c: Tarpit

This is also cool

### Exercise instructions

1. Implement [endlessh](https://nullprogram.com/blog/2019/03/22/)

2. What happens and are we following the RFCs?
    Document using screenshots and PCAPS.

### Links

see above
