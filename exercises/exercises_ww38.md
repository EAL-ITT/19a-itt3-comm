\pagebreak

# Exercises for ww38

DNS

## Exercise 1: DNS and RFC


### Exercise instructions

1. Read up on DNS. It is mostly [rfc1034](https://tools.ietf.org/html/rfc1034), [rfc1035](https://tools.ietf.org/html/rfc1035), [rfc1183](https://tools.ietf.org/html/rfc1183) and [bcp219](https://tools.ietf.org/html/bcp219)

   Focus the RRs and compare with [the wikipedia overview](https://en.wikipedia.org/wiki/List_of_DNS_record_types)

2. Use `dig` command to examine `ucl.dk`

   What is the value for the different RRs, and what does it mean?

Remember to take notes.


### Links

None at this time


## Exercise 2: DNS sniffing

1. Install dnscap in Kali

   Start [here](https://www.dns-oarc.net/tools/dnscap)

   You might need to do `apt-get install -y build-essential pkg-config`

   See [here](https://prefetch.net/blog/2011/01/26/using-dnscap-to-debug-dns-problems-on-linux-hosts/) and [here](https://github.com/DNS-OARC/dnscap/blob/master/README.md)

2. Do some DNS queries and find it both in `dnscap` and `wireshark`

### Links

None at this time

## Exercise 3a: DNS over TLS

1. Read up on DNS-over-TLS. It is [rfc7858](https://tools.ietf.org/html/rfc7858) and [rfc8310](https://tools.ietf.org/html/rfc8310)

1. Install stubby

   Start [here](https://www.techrepublic.com/article/how-to-use-dns-over-tls-on-ubuntu-linux/) and set it to use [censurfridns](https://blog.uncensoreddns.org/dns-servers/)

2. Do some DNS queries and find it both in `dnscap` and `wireshark`


### Links

None at this time

## Exercise 3b: Set up a DNS server

1. Install `unbound`. See [here](https://calomel.org/unbound_dns.html)

   There are untested hints.

2. Configure it to use DNS-over-TLS. See e.g. [here](https://www.ctrl.blog/entry/unbound-tls-forwarding.html)


### Links

None at this time


## Exercise 3c: DNSSEC

1. Read up on DNSSEC. It is [rfc4033](https://tools.ietf.org/html/rfc4033), [rfc4034](https://tools.ietf.org/html/rfc4034) and [rfc4034](https://tools.ietf.org/html/rfc4035)

2. Describe the RRs

3. Check various domains using a validator, e.g. [verisign](https://dnssec-analyzer.verisignlabs.com/eal.dk)

### Links

None at this time


## Exercise 3d: Set up letsencrypt using DNS01

1. Start [here](https://letsencrypt.org/docs/challenge-types/)

### Links

None at this time
