\pagebreak

# Exercises for ww39

SMTP

## Exercise 1: SMTP and RFC


### Exercise instructions

1. Read up on the RFCs. [RFC 5321](https://tools.ietf.org/html/rfc5321) and [RFC 3207](https://tools.ietf.org/html/rfc3207)

   There are way more RFCs related to SMTP. Feel free to read others.

2. Connect to mail.eal.dk using telnet and try to send emails.

   See e.g. [here](https://www.netadmintools.com/telnet-smtp-test)

   Refind the commands in wireshark.

3. Use a mail program, e.g. thunderbird, and set mail.eal.dk as STMP server. Send emails.

   Refind the stream in wireshark.

4. Redo 3. using STARTTLS


### Links

None at this time


## Exercise 2: Install an SMTP server

1. Install and configure exim4

   See [here](https://wiki.debian.org/Exim)

2. Send a test email from localhost

   Verify reception and refind it in the logs.

### Links

None at this time

## Exercise 3: DKIM, SPF, DMARC

1. Read up on it in the RFCs

   * DKIM: [RFC 6376](https://tools.ietf.org/html/rfc6376), [RFC 5585](https://tools.ietf.org/html/rfc5585) and [RFC 5617](https://www.rfc-editor.org/rfc/rfc5617)
   * SPF: [RFC 7208](https://tools.ietf.org/html/rfc7208)
   * DMARC: [RFC 7489](https://tools.ietf.org/html/rfc7489)

2. Use dig to find the corresponding DNS entries for ucl.dk

   Hint: Refind a dkim entry at `k1._domainkey.nyhedsbrev.ucl.dk`


## Exercise 4a: Set up encryption

We have two "forms" of encrypted SMTP: STARTTLS and normal TLS. See [here](https://www.fastmail.com/help/technical/ssltlsstarttls.html)

1. Set up STARTTLS

   See [here](https://www.exim.org/exim-html-current/doc/html/spec_html/ch-encrypted_smtp_connections_using_tlsssl.html)

2. Connect using e.g. thunderbird and refind the communication in wireshark

3. Setup encrypted SMTP (on port 465)

   E.g. see [here](https://www.exim.org/exim-html-current/doc/html/spec_html/ch-encrypted_smtp_connections_using_tlsssl.html)

3. Figure out a way to get to the pre-master secrets and decrypt SMTP traffic using wireshark.

   Spoiler: I didn't fin any exim4 resources for this.


### Links

None at this time

## Exercise 4b: Set up your own domain

1. Install webserver

2. add an MX record

3. test

(more to come, if I have time.)
