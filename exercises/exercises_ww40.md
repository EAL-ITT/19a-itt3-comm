\pagebreak

# Exercises for ww40

VPN, IPSEC, OpenVPN, wireguard

## Exercise 1: vpn.eal.dk og anyconnect

Connect through VPN on vpn.eal.dk. Make it work in kali.

### Exercise instructions

In Kali, as root, do

1. `apt-get install openconnect`
2. `openconnect vpn.eal.dk`

   This might not work if you on school network, since the DNS resolv give a different result.

   Adding an entry in `etc/hosts` will circumvent this. Use `dig` for find the right ip address.

3. Use `wireshark` to see trafic.

4. Use `ip route` to see the routing table

   Which traffic is routed where?

4. Which encryption are used? Which TLS versions? Anyhing else?

### Links

None at this time


## Exercise 2: openvpn

In teams of two, set up a client and server on separate devices.

### Exercise instructions

Follow the guide on the debian wiki.

0. Make a network diagram of what you try to achieve.
1. Describe how to test

   Both `ping` and `traceroute` is relevant

On the server

1. Use a  minimal debian and give it 2 NIC, and configure IP addresses
1. `apt-get install openvpn`
2. Generate static key (don't use certificates)
3. Create the config file

On the client (a kali), as root

1. `apt-get install openvpn`
2. Get the static key
3. Create the config file
4. `openvpn --config /etc/openvpn/tun0.conf --verb 6`

4. Which encryption are used? Anyhing else? What is HMAC?

### Links

Static route as part of the config file, go [here](https://openvpn.net/community-resources/static-key-mini-howto/)

## Exercise 3: wireguard

wireguard is just cooler

### Exercise instructions

Like the openvpn

Go [here](https://www.wireguard.com/#simple-network-interface) for detailed instructions and

1. `apt install wireguard` on both client and server

    Notice that on debian, you need to add an additional repository as per the link above.

2. Set up the host and peer pivate/public addresses

3. Remember to set up routes also.

   Debug using `tcpdump`.

If you get stuff like `RTNETLINK answers: Operation not supported`, the wireguard module might not be correctly installed.
Check using `dpkg-reconfigure wireguard-dkms`. Perhaps you need to install kernel headers - `apt-get install linux-image-amd64 linux-headers-amd64`


### Links

* [Debian help](https://wiki.debian.org/Wireguard)
* [wireguard keys](https://www.wireguard.com/quickstart/)
