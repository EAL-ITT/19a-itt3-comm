---
Week: 35
Content:  Project statup
Material: See links in weekly plan
Initials: MON
---

# Week 35

This week we will be doing what I call introduction to the course, tools and online resources.

Please note that this is an elective course, where teacher attendance is less than usual. We will need to coordinate when the teacher is present and not.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* We have decided on clas how to run the course.

    Ensure you have good communicaoitn channels to MON and your classmates.

* Everybody has a VM with Kali linux

### Learning goals

* The student is able to locate and use common tool for network and protocol analysis.
* The student is able to find reference material related to protocol

## Deliverables

We have distinct group doing presentations for the class

* Group A: Introduction to Kali incl. pointers to installation
* Group B: Kali tool for protocol analysis
* Group C: Overview on RFCs

Groups B and C will focus on HTTP, DNS, SMTP and the others from the weekly list.

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time | Activity |
| :---: | :--- |
| 8:15 | MON introduces the concepts |
| 9:00? |  You work |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 12:30 | Presentations and discussions |


## Hands-on time

See presentation topics above

A word on presentations:

* Always consider audience and purpose (aka. why should they listen to you)
* Presentation are ~20 min, so I expect 5-10 slides with almost no text
* Demos means demos :-)
* You are multiple people in the groups. Not everybody has to stand at the whiteboard presenting.

Do exercises 1 through 3.


## Comments

* RFCs are [here](https://tools.ietf.org/rfc/index)
* kali is [here](https://www.kali.org/)
* It should be obvious from the exercises that I expect you to work independently.
* Also, we want practical stuff, not much theory.
