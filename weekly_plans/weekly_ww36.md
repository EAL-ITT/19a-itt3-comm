---
Week: 36
Content:  HTTP(S) and webdav
Material: See links in weekly plan
Initials: MON
---

# Week 36

This week we look at HTTP, HTTPS and webdav.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* SRX240 connected to the internet (volunteers needed)
* Everybody has a VM with an nginx webserver with both http and https.

### Learning goals

* The student can explain the structure of the HTTP protocol, and can find additional ressources.
* The student can explain the structure of the HTTPS protocol, and can find additional ressources.
* The student know the structure of the HTTP protocol, and can find additional ressources.


## Deliverables

* Demos, presentations and workshops wednesday afternoon.


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday:

| Time | Activity |
| :---: | :--- |
| 8:15 | MON introduces the concepts |
| 9:00? |  You work |

### Wednesday:

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 12:30 | Demos and discussions |


## Hands-on time

I expect demos or workshops of all exercises.

Go [here](https://eal-itt.gitlab.io/19a-itt3-comm/19A_ITT3_comm_exercises.pdf) for exercises


## Comments

* [RFC 5785](https://tools.ietf.org/html/rfc5785) looks interesting...
* The SRX240 in the server room should be set up as an internet facing device with a public ipv4 address.
* Last week, we only discussed presentations and demos (and you did it well). Remind me to mention workshops also.
* For next iteration:
  * Exercise 1 is too large.
  * Installing webdav was an issue, due to modules not being loaded and poor understanding of virtual hosts. We did most of it on the whiteboard.  
* The SRX is up and running on stud.mit-ucl.dk
