---
Week: 37
Content:  Telnet, SSH
Material: See links in weekly plan
Initials: MON
---

# Week 37

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* None at this time

### Learning goals
* The student know of the telnet protocol
* The student can explain the SSH protocol
* The student can implement an SSH server
* The Student can use SSH keys

## Deliverables
* Demos, presentations and workshops wednesday afternoon.


## Schedule

Below is the tentative schedule, which may be changed depending on input fromthe students.

### Tuesday:

| Time | Activity |
| :---: | :--- |
| 8:15 | MON introduces the concepts |
| 9:00 |  You work |

### Wednesday:

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 12:30 | Presentations and discussions |


## Hands-on time

Are we distributing exercises to groups? does this ensure that people work with all exercises?

Go [here](https://eal-itt.gitlab.io/19a-itt3-comm/19A_ITT3_comm_exercises.pdf) for exercises

## Comments
* Ask more questions. We cannot have a scenario like last week MON is surprise that the students are struggling with the exercises.
* Extra reading for the kean student - [bcp195: Recommendations for Secure Use of Transport Layer Security (TLS) and Datagram Transport Layer Security (DTLS)](https://tools.ietf.org/html/bcp195)
