---
Week: 38
Content:  DNS, DNS over TLS, DNS over HTTPS
Material: See links in weekly plan
Initials: MON
---

# Week 38

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* None at this time

### Learning goals
* The student can explain the DNS protocol and its usage
* The student can use relevat tools to troublshoot DNS

## Deliverables
* None at this time


## Schedule

Below is the tentative schedule, which may be changed depending on input fromthe students.

### Tuesday:

| Time | Activity |
| :---: | :--- |
| 8:15 | MON introduces DNS  |
|      |  You work |

### Wednesday:

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 12:30 | Presentations and discussions |


## Hands-on time

Go [here](https://eal-itt.gitlab.io/19a-itt3-comm/19A_ITT3_comm_exercises.pdf) for exercises

## Comments
* None at this time
