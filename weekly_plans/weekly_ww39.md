---
Week: 39
Content:  (S)SMTP, IMAP(S), POP3(S), SPF, DKIM, DMARC
Material: See links in weekly plan
Initials: MON
---

# Week 39

While working with the protocols, it was decided to leave out IMAP and POP. SMTP and associated stuff is way enough.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* None at this time

### Learning goals
* The students can explain the SMTP protocol and how the email system works.
* The students can set up an SMTP server.

## Deliverables
* Hand in on its learning


## Schedule

Below is the tentative schedule, which may be changed depending on input fromthe students.

### Tuesday:

| Time  | Activity |
| :---: | :---     |
| 8:15  | MON introduces the concepts |
| 9:00  | You work |
| 13:00 | QA with MON |


### Wednesday:

| Time  | Activity  |
| :---: | :---      |
| 8:15  | You work  |
| 10:00 | CV course |
| 14:00 | Work on hand-in |


## Hands-on time

Go [here](https://eal-itt.gitlab.io/19a-itt3-comm/19A_ITT3_comm_exercises.pdf) for exercises


## Comments
* None at this time
