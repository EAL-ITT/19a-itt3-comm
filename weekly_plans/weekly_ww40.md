---
Week: 40
Content:  VPN, IPSEC, OpenVPN, wireguard
Material: See links in weekly plan
Initials: MON
---

# Week 40

This week we will look into VPN connections.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* None at this time

### Learning goals
* The student can explain VPNs and the differences between Client->Network and Network->Network topology.
* The studen can setup a VPN tunnel

## Deliverables
* None at this time

## Schedule

Below is the tentative schedule, which may be changed depending on input fromthe students.

### Tuesday:

| Time  | Activity |
| :---: | :--- |
| 8:15  | MON introduces the concepts |
| 9:00  | You work, MON has all-day meetings |

MON will be on riot and such sporadicly, so give it a try, but don't rely on it.

### Wednesday:

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 12:30 | Presentations and discussions |


## Hands-on time

Go [here](https://eal-itt.gitlab.io/19a-itt3-comm/19A_ITT3_comm_exercises.pdf) for exercises


## Comments
* None at this time
