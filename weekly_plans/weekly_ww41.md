---
Week: 41
Content:  Project statup
Material: See links in weekly plan
Initials: MON
---

# Week 41

We do not have lectures or exercises this week since it is the week of the exams.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Exam has been passed, see the [course plan](https://eal-itt.gitlab.io/19A_ITT3_comm_lecture_plan.pdf) for details

### Learning goals
* None

## Deliverables
* None at this time

## Schedule

Below is the tentative schedule, which may be changed depending on input fromthe students.

### Tuesday:

| Time  | Activity |
| :---: | :--- |
| 9:30  | Recap and exam discussions |


### Wednesday:

Nothing planned

## Hands-on time

* None at this time


## Comments
* We have exams on Friday, you should have received something from wiseflow.
